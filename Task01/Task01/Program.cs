﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "";
            var age = 0;

            Console.WriteLine("Please enter your name");
            name = Console.ReadLine();
            Console.WriteLine("Please enter your age");
            age = int.Parse(Console.ReadLine());

            Console.WriteLine($"Your name is {name} and your age is {age}");
            Console.WriteLine("You name " + name + " and your age " + age + "");
        
        }
    }
}
